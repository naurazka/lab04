from django.db import models

EVENT_CATEGORY_CHOICES = [('family','Family'),
                          ('academic','Academic'),
                          ('others','Others'),]
# Create your models here.
class Schedule(models.Model):
    title       = models.CharField(max_length=120)
    date        = models.DateField()
    place       = models.CharField(max_length=120)
    time        = models.TimeField(auto_now= False, auto_now_add= False)
    category    = models.CharField(max_length=100, choices= EVENT_CATEGORY_CHOICES)