from django.shortcuts import render,redirect
from .models import Schedule
from .forms import ScheduleForm
from django.http import HttpResponse

# Create your views here.
def frontpage2(request):
    return render(request, 'frontpage2.html')

def about2(request):
    return render(request, 'about2.html')

def portfolio(request):
    return render(request, 'portofolio.html')

def contact(request):
    return render(request, 'contact.html')

def post_schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'post_schedule.html', {'schedules':schedules} )

def schedule(request):
    if request.method == 'POST':
        form = ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/post_schedule/')

    else:
        form = ScheduleForm()
    return render(request, 'schedule.html', {'form': form})

def delete_schedule(request, id):
    if request.method == 'POST':
        Schedule.objects.filter(id=id).delete()
        return redirect('homepage:post_schedule')
    else:
        return HttpResponse("/GET not allowed")




