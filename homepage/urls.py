from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.frontpage2, name='frontpage2'),
    path('about/', views.about2, name='about2'), 
	path('portfolio/', views.portfolio, name='portfolio'),
	path('contact/', views.contact, name='contact'),
	path('schedule/', views.schedule, name='schedule'),
	path('post_schedule/', views.post_schedule, name='post_schedule'),
	path('post_schedule/clear/<int:id>/', views.delete_schedule, name='delete_schedule'),
		
]